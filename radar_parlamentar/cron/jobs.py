from django_cron import CronJobBase, Schedule
from django.conf import settings
import logging
from importadores.celery_tasks import importar_assincronamente
from datetime import date
from django.core.cache import cache
from analises.analise import AnalisadorTemporal
from analises.grafico import JsonAnaliseGenerator
import os
from modelagem import models

logger = logging.getLogger("radar")

"""
Django Cron documentation:
http://django-cron.readthedocs.io/en/latest/installation.html

Para executar os jobs localmente, executar dentro do container:
python manage.py runcrons
Lembrar de setar export RADAR_IS_PRODUCTION=True antes de make iniciar
"""


class RadarJob(CronJobBase):

    def do(self):
        try:
            self.executar()
        except:
            logger.exception('Registrando traceback de job')
            raise


# Isso é chamado a cada minuto contanto que
# python manage.py runcrons seja constantemente executado
class DemoJob(RadarJob):
    """Para testes"""

    schedule = Schedule(run_every_mins=1)
    code = 'job.DemoJob'    # a unique code

    def executar(self):
        logger.info('DemoJob foi chamado.')
        # x = 1/0 # para testar log de erros


class ImportadorJob(RadarJob):
    """Executado terça às 2h"""

    RUN_AT_TIMES = ['02:00']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'job.ImportadorJob'

    def executar(self):
        logger.info('ImportadorJob foi chamado.')
        weekday = date.today().weekday()
        # segunda é o zero
        if weekday == 1:
            logger.info('ImportadorJob invocando importações.')
            importar_assincronamente.delay('cmsp')
            importar_assincronamente.delay('cdep')
            importar_assincronamente.delay('sen')
        else:
            logger.info('Hoje não é o dia. ImportadorJob só trabalha às terças.')


class CashRefresherJob(RadarJob):
    """Executado diariamente às 1h"""

    RUN_AT_TIMES = ['01:00']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'job.CashRefresherJob'

    def executar(self):
        logger.info('CashRefresherJob foi chamado.')
        cache.clear()
        casas = models.CasaLegislativa.objects.all()
        periodicidades = [periodo[0] for periodo in models.PERIODOS]
        if 'MES' in periodicidades:
            periodicidades.remove('MES')
        for casa in casas:
            for periodicidade in periodicidades:
                logger.info('CashRefresherJob analisando de %s / %s',
                            casa.nome_curto, periodicidade)
                # procedimento conforme também feito em analises/views.py
                cache_key = f"json_analise_{casa.nome_curto}_{periodicidade}"
                analisador = AnalisadorTemporal(casa, periodicidade)
                analise_temporal = analisador.get_analise_temporal()
                gen = JsonAnaliseGenerator(analise_temporal)
                response = gen.get_json()
                logger.info('CashRefresherJob salvando cache de %s / %s',
                            casa.nome_curto, periodicidade)
                cache.set(cache_key, response, settings.CACHE_TIMEOUT)


class DbDumperJob(RadarJob):
    """Executado segunda às 4h"""

    RUN_AT_TIMES = ['04:00']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'job.DbDumperJob'

    def executar(self):
        logger.info('DbDumperJob foi chamado.')
        weekday = date.today().weekday()
        # segunda é o zero
        if weekday == 0:
            logger.info('DbDumperJob fazendo dump do banco.')
            dumper = DbDumper()
            dumper.dump()
        else:
            logger.info('Hoje não é o dia. DbDumperJob só trabalha às segundas.')


class DbDumper():

    DUMP_FILE = "/radar/radar_parlamentar/radar_parlamentar/static/db-dump/radar.sql"

    def dump(self):
        dump_command = 'pg_dump -h postgres -U radar -d radar --inserts -t modelagem_* -f %s' % DbDumper.DUMP_FILE
        os.system(dump_command)
        compress_command = 'bzip2 -9 -f %s' % DbDumper.DUMP_FILE
        os.system(compress_command)
        collectstatic_command = 'python manage.py collectstatic --noinput'
        os.system(collectstatic_command)
