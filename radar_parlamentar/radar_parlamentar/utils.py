from pathlib import Path

def hash_versao():
    return Path('/VERSION').read_text().rstrip()
