from pathlib import Path

from django import template
from django.utils.html import format_html
from radar_parlamentar import utils

register = template.Library()

@register.simple_tag
def versao_radar():
    versao = utils.hash_versao()
    data_versao = Path('/VERSION_DATE').read_text().rstrip()
    versa_abreviada = versao[:7] if versao != "dev-local" else "dev-local"

    content = (f"Versão: <a href='https://gitlab.com/radar-parlamentar/"
               f"radar/commit/{versao}' "
               f"target='_blank'>"
               f"{versa_abreviada}</a> de "
               f"{data_versao}")
    return format_html(content)
