from django.test import TestCase
from radar_parlamentar.templatetags.versao_radar import versao_radar
import re


class VersaoRadarTest(TestCase):

    def setUp(self):
        self.versao_radar = versao_radar()

    def test_versao_radar(self):

        pattern = re.compile(
            "^Versão: <a href='https://gitlab.com/radar-parlamentar/radar/"
            "commit/"
            "(?:[a-z0-9]{40}|dev-local)"  # dev-local ou formato de hash
            "' target='_blank'>"
            "(?:[a-z0-9]{7}|dev-local)"  # devl-local ou hash de 7 digitos
            "</a> de "
            "(?:[0-2][0-9]|3[01])/[01][0-9]/20[0-9]{2}$"  # Data: DD/MM/20AA
        )

        result = re.match(pattern, self.versao_radar)
        self.assertIsNotNone(result)



"^Versão: <a href='https://gitlab.com/radar-parlamentar/radar/commit/(?:[a-z0-9]{40}|dev-local)' target='_blank'>(?:[a-z0-9]{7}|dev-local)</a> de (?:[0-2][0-9]|3[01])/[01][0-9]/20[0-9]{2}$"
