import logging
import os

logger = logging.getLogger("radar")
logger_file_only = logging.getLogger("radar-file-only")


class ExceptionLoggingMiddleware(object):
    """Sem isso, a exceção não é logada pelo logger radar e, portanto, não vai pro arquivo de log"""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # before the view
        response = self.get_response(request)
        # after the view
        return response

    def process_exception(self, request, exception):
        if os.getenv('RADAR_IS_PRODUCTION'):
            # loja traceback no arquivo de log e na saída padrão
            logger.exception('Registrando traceback de ' + request.path)
        else:
            # nesse caso, o Django já estará logando a exceção no console
            # então queremos logar apenas no arquivo
            logger_file_only.exception('Registrando traceback de ' + request.path)
        return None
