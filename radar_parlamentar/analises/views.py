# Copyright (C) 2012, Leonardo Leite
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.
import sys

from django.contrib.admin.views.decorators import staff_member_required
from django.core.cache import cache
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from modelagem import models
from modelagem.utils import StringUtils
from modelagem.utils import MandatoLists
from analises.models import AnaliseAvancadaForm
from .grafico import JsonAnaliseGenerator
from .analise import AnalisadorTemporal
import logging

log = logging.getLogger("radar")


def analises(request):
    return render(request, 'analises.html')


def analise(request, nome_curto_casa_legislativa):

    ano_ini_era = request.GET.get('ano_ini_era')
    ano_fim_era = request.GET.get('ano_fim_era')
    periodicidade = request.GET.get('periodicidade', models.BIENIO)
    palavras_chave = request.GET.get('palavras_chave', '')
    nome_parlamentar = request.GET.get('nome_parlamentar', '')

    # lista de partidos para montar a legenda do gráfico
    partidos = models.Partido.objects.order_by('numero').all()
    casa_legislativa = get_object_or_404(
        models.CasaLegislativa, nome_curto=nome_curto_casa_legislativa)

    form = AnaliseAvancadaForm(request.GET, casa_legislativa)
    problemas_form_analisa_avancada = form.problemas()

    num_votacao = casa_legislativa.num_votacao()
    mandatos = MandatoLists()
    possiveis_inicios = mandatos.anos_iniciais_de_mandatos(casa_legislativa)
    possiveis_fins = list(map(lambda ano: ano+3, possiveis_inicios))
    possiveis_inicios = list(map(lambda ano: str(ano), possiveis_inicios))
    possiveis_fins = list(map(lambda ano: str(ano), possiveis_fins))
    possiveis_inicios.insert(0, "")
    possiveis_fins.insert(0, "")

    return render(request, 'analise.html',
                  {'casa_legislativa': casa_legislativa,
                   'partidos': partidos,
                   'num_votacao': num_votacao,
                   'periodicidade': periodicidade,
                   'palavras_chave': palavras_chave,
                   'ano_ini_era': ano_ini_era,
                   'ano_fim_era': ano_fim_era,
                   'nome_parlamentar': nome_parlamentar,
                   'possiveis_inicios': possiveis_inicios,
                   'possiveis_fins': possiveis_fins,
                   'problemas_form_analisa_avancada': problemas_form_analisa_avancada})


def json_analise(request, nome_curto_casa_legislativa):
    """Retorna o JSON com as coordenadas do gráfico PCA"""

    ano_ini_era = _to_int(request.GET.get('ano_ini_era'))
    ano_fim_era = _to_int(request.GET.get('ano_fim_era'))
    periodicidade = request.GET.get('periodicidade')
    palavras_chave = request.GET.get('palavras_chave')

    cache_key = f"json_analise_{nome_curto_casa_legislativa}_{periodicidade}"
    if palavras_chave:
        cache_key = f"{cache_key}_{palavras_chave}"
    if ano_ini_era:
        cache_key = f"{cache_key}_{ano_ini_era}"
    if ano_fim_era:
        cache_key = f"{cache_key}_{ano_fim_era}"
    log.info("Recuperando cache para: %s", cache_key)
    response = cache.get(cache_key)
    if not response:
        log.info("Cache não encontrado para: %s", cache_key)
        casa_legislativa = get_object_or_404(
            models.CasaLegislativa, nome_curto=nome_curto_casa_legislativa)
        lista_de_palavras_chave = \
            StringUtils.transforma_texto_em_lista_de_string(palavras_chave)
        analisador = AnalisadorTemporal(
            casa_legislativa, periodicidade, lista_de_palavras_chave,
            ano_ini_era, ano_fim_era)
        analise_temporal = analisador.get_analise_temporal()
        gen = JsonAnaliseGenerator(analise_temporal)
        response = gen.get_json()
        log.info("Salvando json (%.3f MB) na chave %s",
                 (sys.getsizeof(response) / (1024*1024)), cache_key)
        cache.set(cache_key, response)
    return HttpResponse(response, content_type='application/json')


def _to_int(str):
    try:
        return int(str)
    except (ValueError, TypeError):
        return None


def lista_de_votacoes(request, nome_curto_casa_legislativa):
    '''Exibe a lista de votações filtradas'''

    periodicidade = request.GET.get('periodicidade', models.BIENIO)
    palavras_chave = request.GET.get('palavras_chave')
    ano_ini_era = _to_int(request.GET.get('ano_ini_era'))
    ano_fim_era = _to_int(request.GET.get('ano_fim_era'))

    casa_legislativa = get_object_or_404(models.CasaLegislativa,
                                         nome_curto=nome_curto_casa_legislativa)
    lista_de_palavras_chave = StringUtils.transforma_texto_em_lista_de_string(palavras_chave)
    analisador = AnalisadorTemporal(casa_legislativa, periodicidade, lista_de_palavras_chave,
                                    ano_ini_era, ano_fim_era)
    votacoes = analisador.votacoes_filtradas()
    datas_votacoes = [v.data for v in votacoes]
    data_primeira_votacao = min(datas_votacoes)
    data_ultima_votacao = max(datas_votacoes)

    return render(request, 'lista_de_votacoes.html',
                  {'casa_legislativa': casa_legislativa,
                   'lista_de_palavras_chave': lista_de_palavras_chave,
                   'votacoes': votacoes,
                   'periodicidade': periodicidade,
                   'data_primeira_votacao': data_primeira_votacao,
                   'data_ultima_votacao': data_ultima_votacao})


@staff_member_required
def clear_cache(request):
    cache.clear()
    return HttpResponse()


def redirect_json_analise(request, nome_curto_casa_legislativa, periodicidade):
    url = (
            "/radar/json/{}/{}/"
            .format(nome_curto_casa_legislativa, periodicidade)
        )
    return HttpResponseRedirect(url)


def redirect_analise(request, nome_curto_casa_legislativa):
    url = (
            "/radar/{}"
            .format(nome_curto_casa_legislativa)
        )
    return HttpResponseRedirect(url)


def redirect_json_analise_p_chave(request, nome_curto_casa_legislativa,
                                  periodicidade, palavras_chave):
    url = (
            "/radar/json/{}/{}/{}"
            .format(nome_curto_casa_legislativa, periodicidade, palavras_chave)
        )
    return HttpResponseRedirect(url)


def redirect_votacoes_filtradas(request, nome_curto_casa_legislativa):
    url = (
            "/votacoes/{}"
            .format(nome_curto_casa_legislativa)
        )
    return HttpResponseRedirect(url)


def redirect_lista_votacoes_p_chave(request, nome_curto_casa_legislativa,
                                    periodicidade, palavras_chave):
    url = (
            "/votacoes/{}/{}/{}"
            .format(nome_curto_casa_legislativa, periodicidade, palavras_chave)
        )
    return HttpResponseRedirect(url)


def redirect_plenaria(request, nome_curto_casa_legislativa,
                      identificador_proposicao):
    url = (
            "/plenaria/{}/{}"
            .format(nome_curto_casa_legislativa, identificador_proposicao)
        )
    return HttpResponseRedirect(url)


def redirect_json_plenaria(request, nome_curto_casa_legislativa,
                           identificador_proposicao):
    url = (
            "/plenaria/json/{}/{}"
            .format(nome_curto_casa_legislativa, identificador_proposicao)
        )
    return HttpResponseRedirect(url)


def redirect_importadores(request):
    return HttpResponseRedirect("/dados/importadores")
