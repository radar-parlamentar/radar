import logging
import xml.dom.minidom
import xml.etree.ElementTree as ET 
from django.core.mail import EmailMessage
import os
from django.conf import settings
from modelagem.models import Partido


admins = [admin[1] for admin in settings.ADMINS]

logger = logging.getLogger("erros_importacao")

class ImportadorHelper:

    @classmethod
    def from_tree(cls, tree_attribute, importador_listener):
        """Chama Listener caso retorno seja None."""

        nome_partido = tree_attribute.strip()
        partido = Partido.from_nome(nome_partido)
        if partido is None:
            importador_listener.partido_nao_encontrado(nome_partido)
            partido = Partido.get_sem_partido()
        return partido

class ImportadorListener:

    def __init__(self):
        self.proposicoes_invalidas = set()
        self.partidos_nao_encontrados = set()

    def on_proposicao_invalida(self, causa, nome_arquivo, xml_etree):
        xml_etree.remove(xml_etree.find('Votos'))
        xml_etree.append(ET.fromstring("""
                <Votos>
                    ...
                </Votos>
                """))
        xml_str = ET.tostring(xml_etree)
        formatted_xml = xml.dom.minidom.parseString(xml_str)

        message = f"""Erro ao salvar votação da sessão: {xml_etree.find('CodigoSessaoVotacao').text}
            Causa: {causa}
            Arquivo: {nome_arquivo}
            XML da votação:
            {formatted_xml.toprettyxml()}"""
        
        self.proposicoes_invalidas.add(message)

        logger.error(message)

    def partido_nao_encontrado(self, nome_partido):
        if nome_partido not in self.partidos_nao_encontrados:
            self.partidos_nao_encontrados.add(nome_partido)
        
            logger.error(
                f"Erro - Partido {nome_partido} não encontrado."
            )
    
    def on_finish_importacao(self, nome_casa_legislativa):
        
        if not settings.EMAIL_ESTA_CONFIGURADO:
            return
        
        message = f'Nova importação de dados foi executada para {nome_casa_legislativa}\n'
        if len(self.partidos_nao_encontrados) != 0:
            message = f"{message}\n{len(self.partidos_nao_encontrados)} partidos não foram encontrados:\n"

        for partido in self.partidos_nao_encontrados:
            message = f'{message}{partido}\n'
        
        if len(self.proposicoes_invalidas) != 0:
            message = f"{message}\nForam encontradas {len(self.proposicoes_invalidas)} proposições inválidas, a informação referente a elas está nos arquivos anexos"
        
        attachments = []
        for index, proposicao in enumerate(self.proposicoes_invalidas):
            attachments.append((f"Proposição inválida {index}.txt", proposicao, "text/plain"))
        mail = EmailMessage(subject=f'Nova importação de dados para {nome_casa_legislativa}',
                            body = message,
                            to=admins,
                            attachments=attachments)
        mail.send()

