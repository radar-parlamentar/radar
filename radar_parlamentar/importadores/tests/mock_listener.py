class ImportadorListenerMock:

    called = False
    causa = None
    arquivo = None
    xml_etree = None
    nome_partido = None
    nome_casa_legislativa = None

    def on_proposicao_invalida(self, causa, arquivo, xml_etree):
        self.called = True
        self.causa = causa
        self.arquivo = arquivo
        self.xml_etree = xml_etree.find('CodigoSessao').text

    def partido_nao_encontrado(self, nome_partido):
        self.nome_partido = nome_partido

    def on_finish_importacao(self, nome_casa_legislativa):
        self.nome_casa_legislativa = nome_casa_legislativa