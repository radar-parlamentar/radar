# Copyright (C) 2012, Leonardo Leite
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.

"""módulo convencao (Convenção Nacional Francesa)

Classes:
    ImportadorConvencao gera dados para casa legislativa fictícia chamada
    Convenção Nacional Francesa
"""

from __future__ import unicode_literals
from django.utils.dateparse import parse_datetime
from modelagem import models
import logging

logger = logging.getLogger("radar")

CASA_LEGISLATIVA_NOME_CURTO = 'conv'

# Eu queria deixar as datas no século de 1700, mas o datetime só lida com
# datas a partir de 1900
ANO_PROPOSICOES = 1988
INICIO_PERIODO = parse_datetime('1989-01-01 0:0:0')
FIM_PERIODO = parse_datetime('1989-12-30 0:0:0')

DATA_NO_PRIMEIRO_SEMESTRE = parse_datetime('1989-02-02 0:0:0')
DATA_NO_SEGUNDO_SEMESTRE = parse_datetime('1989-10-10 0:0:0')
DATA_VOTACAO_9 = parse_datetime('1990-01-01 0:0:0')

PARLAMENTARES_POR_PARTIDO = 3

GIRONDINOS = 'Girondinos'
JACOBINOS = 'Jacobinos'
MONARQUISTAS = 'Monarquistas'


class ImportadorConvencao:

    def _gera_casa_legislativa(self):

        conv = models.CasaLegislativa()
        conv.nome = 'Convenção Nacional Francesa'
        conv.nome_curto = CASA_LEGISLATIVA_NOME_CURTO
        conv.esfera = models.FEDERAL
        conv.local = 'Paris (FR)'
        conv.save()
        return conv

    def _gera_partidos(self):

        girondinos = models.Partido()
        girondinos.nome = GIRONDINOS
        girondinos.numero = 27
        girondinos.cor = '#008000'
        girondinos.save()
        jacobinos = models.Partido()
        jacobinos.nome = JACOBINOS
        jacobinos.numero = 42
        jacobinos.cor = '#FF0000'
        jacobinos.save()
        monarquistas = models.Partido()
        monarquistas.nome = MONARQUISTAS
        monarquistas.numero = 79
        monarquistas.cor = '#800080'
        monarquistas.save()
        "self.partidos = {girondinos, jacobinos, monarquistas}"
        self.partidos = [girondinos, jacobinos, monarquistas]

    def _gera_parlamentares(self):
        # nome partido => lista de parlamentares do partido
        self.parlamentares = {}
        for partido in self.partidos:
            self.parlamentares[partido.nome] = []
            for i in range(0, PARLAMENTARES_POR_PARTIDO):
                parlamentar = models.Parlamentar()
                parlamentar.parlamentar_id = '%s%s' % (partido.nome[0], str(i))
                parlamentar.nome = 'Pierre'
                parlamentar.casa_legislativa = self.casa
                parlamentar.partido = partido
                parlamentar.save()
                self.parlamentares[partido.nome].append(parlamentar)

    def _gera_proposicao(self, num, descricao):

        prop = models.Proposicao()
        prop.id_prop = num
        prop.sigla = 'PL'
        prop.numero = num
        prop.ano = ANO_PROPOSICOES
        prop.ementa = descricao
        prop.descricao = descricao
        prop.casa_legislativa = self.casa
        prop.save()
        return prop

    def _gera_votacao(self,
                      data,
                      numero_proposicao,
                      descricao_proposicao,
                      votos_girondinos,
                      votos_jacobinos,
                      votos_monarquistas):

        votacao = models.Votacao()
        votacao.id_vot = numero_proposicao
        votacao.descricao = descricao_proposicao
        votacao.data = data
        votacao.proposicao = self._gera_proposicao(numero_proposicao, descricao_proposicao)
        votacao.save()

        votos_girondinos = votos_girondinos
        self._gera_votos(votacao, GIRONDINOS, votos_girondinos)

        votos_jacobinos = votos_jacobinos
        self._gera_votos(votacao, JACOBINOS, votos_jacobinos)

        votos_monarquistas = votos_monarquistas
        self._gera_votos(votacao, MONARQUISTAS, votos_monarquistas)

    def _gera_votos(self, votacao, nome_partido, opcoes):
        # opcoes é uma lista de opções (SIM, NÃO ...)
        for i in range(0, PARLAMENTARES_POR_PARTIDO):
            voto = models.Voto()
            voto.parlamentar = self.parlamentares[nome_partido][i]
            voto.opcao = opcoes[i]
            voto.votacao = votacao
            voto.save()

    # votação com atributos diferentes para teste
    def _gera_votacao8(self):

        numero_proposicao = '8'
        descricao_proposicao = 'Guerra contra a Inglaterra'
        prop = models.Proposicao()
        prop.id_prop = numero_proposicao
        prop.sigla = 'PL'
        prop.numero = numero_proposicao
        prop.ano = ANO_PROPOSICOES
        prop.ementa = 'o uso proibido de armas químicas'
        prop.descricao = 'descricao da guerra'
        prop.casa_legislativa = self.casa
        prop.indexacao = 'bombas, efeitos, destruições'
        prop.save()

        self._gera_votacao(DATA_NO_SEGUNDO_SEMESTRE,
                           numero_proposicao,
                           descricao_proposicao,
                           [models.NAO, models.NAO, models.NAO],
                           [models.ABSTENCAO, models.NAO, models.NAO],
                           [models.SIM, models.AUSENTE, models.SIM])

    def importar(self):

        self.casa = self._gera_casa_legislativa()
        self._gera_partidos()
        self._gera_parlamentares()
        self._gera_votacao(DATA_NO_PRIMEIRO_SEMESTRE,
                           '1',
                           'Reforma agrária',
                           [models.SIM, models.ABSTENCAO, models.NAO],
                           [models.SIM, models.SIM, models.SIM],
                           [models.NAO, models.NAO, models.NAO])
        self._gera_votacao(DATA_NO_PRIMEIRO_SEMESTRE,
                           '2',
                           'Aumento da pensão dos nobres',
                           [models.NAO, models.NAO, models.NAO],
                           [models.NAO, models.NAO, models.NAO],
                           [models.SIM, models.SIM, models.SIM])
        self._gera_votacao(DATA_NO_PRIMEIRO_SEMESTRE,
                           '3',
                           'Institui o Dia de Carlos Magno',
                           [models.NAO, models.NAO, models.SIM],
                           [models.NAO, models.NAO, models.NAO],
                           [models.SIM, models.SIM, models.SIM])
        self._gera_votacao(DATA_NO_PRIMEIRO_SEMESTRE,
                           '4',
                           'Diminuição de impostos sobre a indústria',
                           [models.SIM, models.SIM, models.SIM],
                           [models.SIM, models.ABSTENCAO, models.NAO],
                           [models.SIM, models.NAO, models.AUSENTE])
        self._gera_votacao(DATA_NO_SEGUNDO_SEMESTRE,
                           '5',
                           'Guilhotinar o Conde Pierre',
                           [models.SIM, models.SIM, models.ABSTENCAO],
                           [models.SIM, models.SIM, models.SIM],
                           [models.NAO, models.NAO, models.NAO])
        self._gera_votacao(DATA_NO_SEGUNDO_SEMESTRE,
                           '6',
                           'Criação de novas escolas',
                           [models.SIM, models.SIM, models.SIM],
                           [models.SIM, models.SIM, models.SIM],
                           [models.AUSENTE, models.SIM, models.SIM])
        self._gera_votacao(DATA_NO_SEGUNDO_SEMESTRE,
                           '7',
                           'Aumento do efetivo militar',
                           [models.SIM, models.SIM, models.ABSTENCAO],
                           [models.SIM, models.SIM, models.SIM],
                           [models.SIM, models.AUSENTE, models.SIM])
        self._gera_votacao8()
        self._gera_votacao(DATA_VOTACAO_9,
                           '9',
                           'Contratar médicos para a capital',
                           [models.SIM, models.SIM, models.ABSTENCAO],
                           [models.SIM, models.SIM, models.SIM],
                           [models.SIM, models.AUSENTE, models.SIM])
        self._gera_proposicao('10', 'Legalizacao da maconha')


def main():

    logger.info('LIMPANDO DADOS DA CONVENÇÃO NACIONAL FRANCESA')
    models.CasaLegislativa.deleta_casa(CASA_LEGISLATIVA_NOME_CURTO)
    logger.info('IMPORTANDO DADOS DA CONVENÇÃO NACIONAL FRANCESA')
    importer = ImportadorConvencao()
    importer.importar()
