# Description

`linter_http_link` function abstracts the following process:
- determine the service link points to, currently supported:
  - internal gitlab instance (gitlab.otters.xyz)
  - GitHub
  - generic
- convert link to proper API location of the service
- perform **authenticated** request to determine if link is valid
- TODO: return body for caller consumption (?)

# Current limitations and assumptions
 - Supports only `https://` schemes for gitlab and github. This is by design, because we're sending tokens there.
 - Treats every repo as private. We don't know in advance if repo is private or public, so we're always authenticating
   and always sending `GITLAB_TOKEN` to gitlab and `GITHUB_TOKEN` to github.
 - Supports only master branch, as there's no reliable way to separate branch from path, as branch can contain forward slashes.
 - Bails out if encounters queries in gitlab/github URLs as a precaution. Let me know if there are examples where this is needed.

# Usage

```bash
# source function
source /usr/local/lib/functionarium/linter_http_link.sh

# check gitlab link
export GITLAB_TOKEN="token-with-API-scope"
linter_http_link 'https://gitlab.domain.com/group/project/file.txt'

# check github link
export GITHUB_TOKEN="token-with-repo-scope"
linter_http_link 'https://github.com/group/project/file.txt'

# check generic link
linter_http_link 'https://www.latlmes.com/arts/return-of-the-golden-age-of-comics-1'
```

# Local usage

You can use the same function locally too to shorten the loop and catch those errors before they reach pipeline.
It will complain if it doesn't have required environment variables with token.
