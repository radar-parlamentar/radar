#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Defines function that shellchecks all the things

function linter_shellcheck {
	# Note: hardcode function name rather than calling ${FUNCNAME[0]}
	# as it will fail on different shells
	local p="linter_shellcheck: error:"

	# Check that required command are installed before doing anything
	command -v shellcheck >/dev/null 2>&1 || { >&2 echo "${p} shellcheck is not installed!"; return 1; }

	local _check="${1:-.}"

	if [[ -f "${_check}" ]]; then
		shellcheck --external-sources "${_check}"
		return $?
	elif [[ -d "${_check}" ]]; then
		find "${_check}" \
			-type f \
			\( -name '*.sh' -o -name '*.bash' \) \
			-print0 \
			| xargs -0 -r shellcheck --external-sources
		return $?
	else
		>&2 echo "${p} \$1 should exist, and should be either shell file or directory where to look for shell files"
		return 42
	fi
}
