# Description

`linter_shellcheck` function abstracts the following process:
 - find all the files with `sh` extension, either from current directory or descending from `$1`
 - run shellcheck on each of them via xargs, **following external sources**

# Usage

```bash
# source function
source /usr/local/lib/functionarium/linter_shellcheck.sh

# check all files looking from current directory
linter_shellcheck .
```

# Local usage

You can use the same function locally too to check all your dirty little scripts :)

# Local usage with docker
If you don't want to or have trouble installing individual components for this function,
you can run it with docker, same way as CI does:
 - make sure you can [pull images][2] from internal registry
 - run the image that runs this function (it should have all the dependencies preinstalled)

Example running `linter_shellcheck` on every local file in current directory:
```bash
# pull latest image
docker pull gitlab.otters.xyz:5005/product/systems/dockerfiles/docker-ci-promtool/docker-ci-promtool:latest
# start with local dir mapped to `/build` inside container
docker run -v "$(pwd):/build" -it gitlab.otters.xyz:5005/product/systems/dockerfiles/docker-ci-promtool/docker-ci-promtool:latest
# inside container, source the linter function
source /usr/local/lib/functionarium/linter_shellcheck.sh
# run it, passing /build directory as a parameter
linter_shellcheck /build/
```
You'll see the exact errors as CI would report.

[2]: https://gitlab.otters.xyz/infrastructure/development-environment/blob/master/README.md#how-do-i-login-to-registry
