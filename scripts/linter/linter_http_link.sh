#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Defines function that checks whether link is alive or not

function linter_http_link {
	# Note: hardcode function name rather than calling ${FUNCNAME[0]}
	# as it will fail on different shells
	local p="linter_http_link: error:"
	local url scheme
	local gitlab="${GITLAB_DOMAIN:-gitlab.otters.xyz}"

	# Check that required command are installed before doing anything
	command -v curl >/dev/null 2>&1 || { >&2 echo "${p} curl is not installed!"; return 1; }

	if [[ "unset" == "${1:-unset}" ]];then
		>&2 echo "${p} can't check url if there is none!"; return 42;
	fi

	url="${1}"
	if [[ "${url}" != *"://"* ]]; then
		>&2 echo "${p} URL scheme should be present!"; return 42;
	fi

	url="${url%%#*}"	# remove anchors immediately, we don't care about client side

	scheme="${url%%://*}"
	if [[ "${scheme}" != "https" ]]; then
		>&2 echo "${p} URL scheme should be https, got '${scheme}'!"; return 42;
	fi

	# Handle gitlab-specific API call if hostname equals predefined gitlab value
	# Example URL conversion for gitlab:
	#    from: https://gitlab.otters.xyz/product/systems/prometheus/prometheus-config/blob/master/README.md
	#      to: https://gitlab.otters.xyz/api/v4/projects/product%2Fsystems%2Fprometheus%2Fprometheus-config/repository/files/README.md?ref=master
	if [[ "${url}" == "https://${gitlab}/"* ]]; then
		# Check token first
		if [[ "unset" == "${GITLAB_TOKEN:-unset}" ]]; then
			>&2 echo "${p} Requested gitlab url without exported GITLAB_TOKEN!"; return 42;
		fi

		# Gitlab sanity checks
		if [[ "${url}" != *"/blob/master/"* ]]; then
			# there's no way I'm parsing branch/names/that/can/look/like/paths with shell
			# plus, for our use case there's not much value in checking non-master resources
			>&2 echo "${p} only master branches are supported for gitlab URLs, got '${url}'!"; return 42;
		fi

		if [[ "${url}" != "${url%%\?*}" ]]; then
			# there's a query in url. I'm not sure we can reliably support those, so until
			# tested this is treated as error
			>&2 echo "${p} queries are not supported for gitlab URLs!"; return 42;
		fi

		local uri project path
		uri="${url#https://${gitlab}/}"		# remove scheme and hostname
		project="${uri%%/blob/master/*}"	# use /blob/master/ as separator and get first half
		path="${uri##*/blob/master/}"		# get remainder

		# urlencode for dummies
		project="${project//\//%2F}"
		path="${path//\//%2F}"
		local api_url http_code

		api_url="https://${gitlab}/api/v4/projects/${project}/repository/files/${path}?ref=master"
		http_code="$(curl -sSLI -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -o /dev/null -w "%{http_code}" "${api_url}")"

		if [[ "200" != "${http_code}" ]]; then
			>&2 echo "${p} Error code: '${http_code}', url: '${url}', checked via api url '${api_url}'!"; return 1;
		fi

	# Handle github-specific API call if hostname equals github.com
	# Example URL conversion for github:
	#    from: https://github.com/cabify/product-runbooks/blob/master/runbooks/prometheus/prometheus.md#blah
	#      to: https://api.github.com/repos/cabify/product-runbooks/contents/runbooks/prometheus/prometheus.md
	elif [[ "${url}" == "https://github.com/"* ]]; then
		# Check token first
		if [[ "unset" == "${GITHUB_TOKEN:-unset}" ]]; then
			>&2 echo "${p} Requested github url without exported GITHUB_TOKEN!"; return 42;
		fi

		# GitHub sanity checks
		if [[ "${url}" != *"/blob/master/"* ]]; then
			# there's no way I'm parsing branch/names/that/can/look/like/paths with shell
			# plus, for our use case there's not much value in checking non-master resources
			>&2 echo "${p} only master branches are supported for github URLs, got '${url}'!"; return 42;
		fi

		if [[ "${url}" != "${url%%\?*}" ]]; then
			# there's a query in url. I'm not sure we can reliably support those, so until
			# tested this is treated as error
			>&2 echo "${p} queries are not supported for github URLs!"; return 42;
		fi

		local uri owner_repo path
		uri="${url#https://github.com/}"	# remove scheme and domain
		owner_repo="${uri%%/blob/master/*}"	# use /blob/master/ as separator and get first half
		path="${uri##*/blob/master/}"		# get remainder

		# craft api url
		local api_url http_code
		api_url="https://api.github.com/repos/${owner_repo}/contents/${path}"
		http_code="$(curl -sSLI -H "Authorization: token $GITHUB_TOKEN" -o /dev/null -w "%{http_code}" "${api_url}")"

		if [[ "200" != "${http_code}" ]]; then
			>&2 echo "${p} Error code: '${http_code}', url: '${url}', checked via api url '${api_url}'!"; return 1;
		fi

	# Handle generic API call, let curl do all the validation
	# Pay attention to not leak any tokens here
	else
		http_code="$(curl -sSLI -o /dev/null -w "%{http_code}" "${url}")"

		if [[ "200" != "${http_code}" ]]; then
			>&2 echo "${p} Error code: '${http_code}', url: '${url}', checked directly!"; return 1;
		fi

	fi
}
