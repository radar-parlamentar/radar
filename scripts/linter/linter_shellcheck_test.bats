#!/usr/bin/env bats
# vim: ft=sh:ts=8:sw=8:noet

# General tests
@test "linter_shellcheck: is a function" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& type -t linter_shellcheck"
	[[ "${lines[0]}" = 'function' ]]
	[[ 0 -eq "${status}" ]]
}

@test "linter_shellcheck: does not set extra envvars after being sourced" {
	run env -i bash -c "env_pre=\$(export); \
		source ./fun/linter_shellcheck.sh; \
		env_post=\$(export); \
		diff <(echo \"\${env_pre}\" | grep -v _ps1_cmd) <(echo \"\${env_post}\" | grep -v _ps1_cmd)"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

# Invocation tests
# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_shellcheck: fails when shellcheck is not installed, writes nothing to stdout" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& unset PATH \
				&& linter_shellcheck 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 1 -eq "${status}" ]]
}
@test "linter_shellcheck: fails when shellcheck is not installed, writes error to stderr" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& unset PATH \
				&& linter_shellcheck >/dev/null"
	[[ "${output}" = "linter_shellcheck: error: shellcheck is not installed!" ]]
	[[ 1 -eq "${status}" ]]
}

@test "linter_shellcheck: runs without parameters" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& linter_shellcheck"
	[[ 0 -eq "${status}" ]]
}

@test "linter_shellcheck: runs w/ parameter set to current dir" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& linter_shellcheck ."
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

@test "linter_shellcheck: runs w/ parameter set to existing file" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& echo '#!/bin/bash' > /tmp/test-linter.sh \
				&& linter_shellcheck /tmp/test-linter.sh \
				; rm -f /tmp/test-linter.sh"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

@test "linter_shellcheck: follows external sources" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& echo 'function _f { return 0; }' > /tmp/test-source.sh \
				&& echo -e '#!/bin/bash\nsource /tmp/test-source.sh' > /tmp/test-linter.sh \
				&& linter_shellcheck /tmp/test-linter.sh \
				; rm -f /tmp/test-linter.sh /tmp/test-source.sh"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_shellcheck: fails w/ parameter set to nonexistent dir/file, writes nothing to stdout" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& linter_shellcheck /tmp/nonexistent-${RANDOM} 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_shellcheck: fails w/ parameter set to nonexistent dir/file, writes errors to stderr" {
	run env -i bash -c "source ./fun/linter_shellcheck.sh \
				&& linter_shellcheck /tmp/nonexistent-${RANDOM} >/dev/null"
	echo "z${output}z"
	[[ "${output}" = "linter_shellcheck: error: \$1 should exist, and should be either shell file or directory where to look for shell files" ]]
	[[ 42 -eq "${status}" ]]
}
