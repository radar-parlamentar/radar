#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Tenta baixar alguma(s) imagem(ns) para usar o cache dos layers e acelerar o
# build.
docker pull "${IMAGEM_TESTE}:${TAG}" || \
    docker pull "${IMAGEM_PRODUCAO}:latest" ||
    true

CACHE_FROM=""

# Tenta descobrir qual foi a imagem baixada, se alguma foi:
if [[ -n "$(docker image ls --filter "reference=${IMAGEM_TESTE}" -q)" ]]; then
	CACHE_FROM="$(docker image ls --filter "reference=${IMAGEM_TESTE}" --format "{{.CreatedAt}} {{.Repository}}:{{.Tag}}" | sort | tail -n1  | awk '{ print $5 }')"
elif [[ -n "$(docker image ls --filter "reference=${IMAGEM_PRODUCAO}" -q)" ]]; then
	CACHE_FROM="$(docker image ls --filter "reference=${IMAGEM_PRODUCAO}" --format "{{.CreatedAt}} {{.Repository}}:{{.Tag}}" | sort | tail -n1  | awk '{ print $5 }')"
fi

if [[ -n "${CACHE_FROM}" ]]; then
	docker build \
		--cache-from "${CACHE_FROM}" \
		-t "${IMAGEM_TESTE}:${TAG}" \
		-f "${DOCKERFILES}/Main" \
		--build-arg RADAR_VERSION="${RADAR_VERSION}" \
		--build-arg VERSION_DATE="${VERSION_DATE}" \
		.
else
	docker build \
		-t "${IMAGEM_TESTE}:${TAG}" \
		-f "${DOCKERFILES}/Main" \
		--build-arg RADAR_VERSION="${RADAR_VERSION}" \
		--build-arg VERSION_DATE="${VERSION_DATE}" \
		.
fi

if [[ "${GITLAB_CI:-false}" == "true" ]]; then
	docker push "${IMAGEM_TESTE}:${TAG}"
fi
