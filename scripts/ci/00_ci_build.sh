#!/bin/bash
# vim: ai:ts=8:sw=8:noet
docker build \
	-t "${IMAGEM_CI}:${TAG}" \
	-f "${DOCKERFILES}/CI" \
	.

if [[ "${GITLAB_CI:-false}" == "true" ]]; then
	docker push "${IMAGEM_CI}:${TAG}"
fi
