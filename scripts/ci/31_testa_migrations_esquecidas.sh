#!/bin/bash
# vim: ai:ts=8:sw=8:noet

export RADAR_IMAGE="${IMAGEM_TESTE}:${TAG}"
export RADAR_TEST="TRUE"

_IMAGEM_PRESENTE="$(docker image ls "${RADAR_IMAGE}" -q)"

if [[ -z "${_IMAGEM_PRESENTE}" ]]; then
	docker pull "${RADAR_IMAGE}"
fi

# Como ainda estamos usando sqlite para testes, não precisamos de nenhum outro
# container ou serviço, então vamos rodar direto a aplicação.
docker run \
	--rm \
	--name radar \
	-e RADAR_TEST=TRUE \
	--entrypoint="python" \
	"${RADAR_IMAGE}" \
	manage.py \
		makemigrations \
		--no-input \
		--check

echo "Ok, todas as migrations estão sincronizadas com a última versão."
