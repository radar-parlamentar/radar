#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Gera algumas variáveis que precisamos;
# Define.
set -eufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

umask 0077	# by default create only owner-readable files

CALL=$@

export INIT_DIR="${PWD}"
ROOT_DIR="$(git rev-parse --show-toplevel)"
export ROOT_DIR="${ROOT_DIR}"
export SCRIPTS="${ROOT_DIR}/scripts"

# shellcheck disable=SC1090
. "${SCRIPTS}/variables.sh"

### Definindo funções principais

function _cmd_check() {
	cmd="$1"
	command -v "${cmd}" >/dev/null 2>&1 \
		|| { echo "${cmd} is required and is not installed, exiting!"; \
			exit 42; }
}
export -f _cmd_check

function cmd_checks() {
	_cmd_check "git"
	_cmd_check "bash"
}
export -f cmd_checks

function cleanup() {
	local STATUS=$?
	if [[ "${STATUS}" != "0" ]]; then
		echo
		echo "###################################################"
		echo "A execução do "'"'"${CALL}"'"'" falhou."
		echo "###################################################"
		echo
	fi
	cd "${INIT_DIR}"
	echo "Limpando a pasta temporária do radar ${RADAR_TEMP_DIR}..."
	rm -rf "${RADAR_TEMP_DIR}"
	# shellcheck disable=SC2086
	exit ${STATUS}
}
export -f cleanup

function ci_only() {
	if [[ "true" != "${GITLAB_CI:-false}" ]]; then
		echo "Rodando script de CI fora do CI, terminando."
		exit 42
	fi
}
export -f ci_only

function call_linter() {
	_cmd_check "$1"
	# shellcheck disable=SC1090
	. "${LINTERS}/linter_$1.sh"
}
export -f call_linter

function usage() {
	echo "As opções estágios do ci disponíveis são:"
	SCRIPTS="$(find "${CI_SCRIPTS}" -type f \
		| grep -v 'run_ci' \
		| sort -n \
		| sed 's/.*scripts\/ci\/[0-9][0-9]_\(.*\)\.sh/\1/g')"
	for S in ${SCRIPTS}; do
		echo "  - ${S}"
	done
	echo "E você ainda tem o comando 'linter' com as opções:"
	MY_LINTERS="$(find "${LINTERS}" -type f \
		| grep -E '\.sh$' \
		| sed 's/.*scripts\/linter\/linter_\(.*\)\.sh/\1/g')"
	for L in ${MY_LINTERS}; do
		echo "  - ${L}"
	done
}

function bad_usage() {
	echo "Nenhuma opção passada ou opção inválida."
	usage
	exit 42
}

function main() {
	FUNCAO=$(echo "$1" | cut -d' ' -f1)
	ARGUMENTO=$(echo "$1" | sed 's/'${FUNCAO}'\s\?//g')
	cd "${ROOT_DIR}"
	case ${FUNCAO} in
		ci_build)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/00_${FUNCAO}.sh"
			;;
		ci_release)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/01_${FUNCAO}.sh"
			;;
		teste_build)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/20_${FUNCAO}.sh"
			;;
		nginx_build)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/21_${FUNCAO}.sh"
			;;
		testa_aplicacao)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/30_${FUNCAO}.sh"
			;;
		testa_migrations_esquecidas)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/31_${FUNCAO}.sh"
			;;
		testa_migrations_modificadas_manualmente)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/32_${FUNCAO}.sh"
			;;
		producao_release)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/40_${FUNCAO}.sh"
			;;
		nginx_release)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/41_${FUNCAO}.sh"
			;;
		implantacao)
			# shellcheck disable=SC1090
			bash "${CI_SCRIPTS}/50_${FUNCAO}.sh"
			;;
		linter)
			if [[ -z "${ARGUMENTO}" ]]; then 
				bad_usage
			fi
			call_linter "${ARGUMENTO}"
			;;
		help)
			usage
			exit 0
			;;
		*)
			bad_usage
			;;
	esac
}

##############################################################################
# Começando as execuções
##############################################################################

# Gearante uma limpeza no final da execução.
# shellcheck disable=SC1090
trap cleanup EXIT

# Checando se um argumento foi passado.
test $# -ge 1  2>&1 || bad_usage

# Checando existência de alguns programas necessários
cmd_checks

# Chamando função principal
main ${CALL}

echo
echo "Ok, encerrando por aqui!"
