#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Run this script only when in CI
ci_only

# Validando variáveis de ambiente necessárias:
while IFS=' ' read -r VARIAVEL SECRET; do
	if [[ "${!VARIAVEL:-undefined}" == "undefined" ]]; then
		echo "Você precisa definir a variável: ${VARIAVEL}"
		exit 42
	elif [[ "${SECRET}" == "FALSE" ]]; then
		echo " - ✔ ${VARIAVEL}: ${!VARIAVEL}"
	else
		echo " - ✔ ${VARIAVEL}: <<hidden>>"
	fi
done <<-EOF
	CI_COMMIT_SHA FALSE
	IMAGEM_PRODUCAO FALSE
	IMAGEM_NGINX FALSE
	RADAR_DB_PASSWORD TRUE
	REGISTRY FALSE
	SSH_HOST TRUE
	SSH_USER TRUE
	TAG FALSE
EOF


export RADAR_IMAGE="${IMAGEM_PRODUCAO}:${TAG}"
export RADAR_NGINX_IMAGE="${IMAGEM_NGINX}:${TAG}"

# Prepara chave ssh

function cleanup_ssh_key(){
	local STATUS=$?

	# Limpando pasta ssh temporária
	rm -rf "${HOME}/.ssh"
	# Restaurante pasta anterior caso ela exista
	if [[ -d "${HOME}/ssh_bkp" ]]; then
	    mv "${HOME}/.ssh_bkp" "${HOME}/.ssh"
	fi

	exit ${STATUS}
}

# Faz um backup da pasta ${HOME}/.ssh, caso exista
if [[ -d "${HOME}/.ssh" ]]; then
    mv "${HOME}/.ssh" "${HOME}/.ssh_bkp"
fi

# Estou adicionando isso aqui para garantir que em qualquer hipótese nós vamos
# limpar a pasta ssh do runner, para evitar vazar nossa chave ssh privada.
trap cleanup_ssh_key EXIT

# Garantindo que o agent ssh está "conectado" à nossa sessão
eval "$(ssh-agent -s)"

# Adicionando nossa chave privada ao agent ssh
# de forma que não vamos vazar ele no console
echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -

mkdir -p "${HOME}/.ssh"

chmod 700 "${HOME}/.ssh"

# Adiciona chave ssh do servidor para o chaveiro como uma chave conhecida.
ssh-keyscan "${SSH_HOST}" >> "${HOME}/.ssh/known_hosts"

chmod 644 "${HOME}/.ssh/known_hosts"

# Agora sim rodando o deploy lá no servidor do radar
set +u
# shellcheck disable=SC2087
ssh -T "${SSH_USER}@${SSH_HOST}" <<-CODE
	echo "Logado no servidor."
	set -eufCo pipefail
	# Disabling shell history
	set +uo history
	if [[ ! -d "/home/radar/radar" ]]; then
		echo "Clonando o projeto no servidor."
		git clone "${CI_PROJECT_URL}.git" "/home/radar/radar"
		cd /home/radar/radar
	fi
	cd /home/radar/radar
	echo "Configurando variáveis de ambiente"
	export DOCKER_REGISTRY=${REGISTRY}
	export RADAR_IMAGE=${RADAR_IMAGE}
	export RADAR_NGINX_IMAGE=${RADAR_NGINX_IMAGE}
	export RADAR_IS_PRODUCTION=True
	export RADAR_DB_PASSWORD=${RADAR_DB_PASSWORD}
	export RADAR_EMAIL_PASSWORD=${RADAR_EMAIL_PASSWORD}
	git fetch --all --prune --tags
	# Limpando quaisquer mudanças que tenham sido feitas manualmente.
	git reset --hard HEAD
	# Baixando a última versão do código do projeto
	git checkout ${CI_COMMIT_SHA}
	# Atualizando a stack do radar
	docker stack deploy -c radar.yml -c radar_prod.yml radar
	sleep 5
	# Recarrega o nginx para usar o certificado SSL atualizado,
	# caso o letsencrypt tenha atualizado o mesmo.
	docker service update --force radar_nginx
	# Fazendo algumas limpezas para evitar "sobrecarga" no servidor
	docker container prune -f
	sleep 5
	docker image prune -a -f
	# Reenable shell history
	set -o history
CODE

# Verifica se a nova versão está implantada
URL="https://radarparlamentar.polignu.org/versao?invalida_cache=$(date +%s)"
TENTATIVAS=5
count=1
echo "Dando um tempo antes de verificar a implantação."
sleep 60 # segundos
response_body=""
while [[ "${response_body}" != "${CI_COMMIT_SHA}" && ${count} -lt ${TENTATIVAS} ]]
do
  sleep 20
  echo "Verificando implantação acessando $URL"
  response_body="$(curl --insecure -s "${URL}")"
  count=$((count+1))
done
if [[ "${response_body}" == "${CI_COMMIT_SHA}" ]]
then
  echo "Implantação realizada com sucesso!"
else
  echo "A implantação da nova versão falhou."
  curl --insecure -is "${URL}"
  exit 1
fi
