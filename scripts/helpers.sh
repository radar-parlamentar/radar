#!/bin/bash
# vim: ai:ts=8:sw=8:noet
set -eufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

function swarm_init() {
	echo "Garantindo que o docker swarm está rodando."
	if [[ "$(docker info --format '{{.Swarm.LocalNodeState}}')" == "inactive" ]]; then
		docker swarm init --advertise-addr 127.0.0.1
	fi
}

export -f swarm_init

function swarm_stop() {
	echo "Garantindo a parada do docker swarm."
	if [[ "$(docker info --format '{{.Swarm.LocalNodeState}}')" == "inactive" ]]; then
		echo "O docker swarm não estava ativo."
		return
	fi

	# Verificando se temos uma stack do radar rodando.
	if [[ -n "$(docker stack ls --format '{{.Name}}' | grep radar)" ]]; then
		echo "Parando a stack do radar."
		docker stack down radar
	fi

	# Verificando se não tem alguma outra stack rodando
	if [[ -z "$(docker stack ls --format '{{.Name}}')" ]]; then
		echo "Parando o swarm."
		docker swarm leave --force
	else
		echo "Existem outras stacks rodando no swarm, não vamos pará-lo."
	fi
}

export -f swarm_stop

# --- End Definitions Section ---    
# check if we are being sourced by another script or shell
[[ "${#BASH_SOURCE[@]}" -gt "1"  ]] && { return 0; }
# --- Begin Code Execution Section ---

if [[ $# -ne 0 ]]; then
	$@
fi
