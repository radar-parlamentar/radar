DADOS ABERTOS
---------------

* [Câmara dos deputados](http://www2.camara.gov.br/transparencia/dados-abertos)
* [Câmara municipal de São Paulo](http://www.camara.sp.gov.br/index.php?option=com_wrapper&view=wrapper&Itemid=219)

ESTILO DE PROGRAMAÇÃO PYTHON DO GOOGLE:
-----------------------------------------

* [Google PyGuide](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html) **vamos *tentar* seguir**

DEPLOY E DJANGO
-----------------

O framework base do projeto é o Django, então é bom entender um pouco de como
ele funciona. Porém, iniciamos o projeto em 2012, com django 1.4, e a
arquitetura/organização de arquivos em projetos django mudou muito de lá pra
cá. Então nossa organização não segue completamente os padrões recomendados
pelo framework. Além disso, nossos "módulos" não foram feitos para serem
distribuídos de forma independente, como "Apps Django", mais um motivo para não
seguirmos o padrão _ipsis literis_. De qualquer maneira, conhecer o django
ajuda!

* [Tutorial de Django](https://docs.djangoproject.com/en/dev/intro/tutorial01/)

Outra tecnologia que utilizamos no projeto é o Docker. Mais do que isso, nossa
organização atual do projeto está descrita em arquivos no formato "Compose" do
Docker para ser executado usando o orquestrador "Swarm", _built-in_ do próprio
docker. Antigamente nós utilizávamos o *docker-compose* para fazer a gestão dos
containers, mas o *Swarm* é mais completo/poderoso e não requer a instalação de
uma dependência extra.
