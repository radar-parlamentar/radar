# Configuração do ambiente

## 1. Clonando o repositório

A primeira coisa que deve ser feita é o clone do projeto no *Gitlab*. Para isso
basta fazer:

`$ git clone https://gitlab.com/radar-parlamentar/radar.git``

## 2. Configuração do ambiente

O Radar Parlamentar funciona, tanto em produção quanto em desenvolvimento,
utilizando containers docker.

Para tanto, você precisa ter o *Docker* instalado em seu sistema operacional.
Nossa instalação foi testada utilizando:

* Docker 19.03.5 build 633a0ea838

Além do Docker, também temos um Makefile para ajudar o desenvolvedor,
simplificando alguns comandos e procedimentos. Dessa forma, se você quiser
utilizar esse nosso facilitador você precisará instalar o *make* em seu
ambiente também.

## 3. Rodar o projeto

Como dito, temos um *Makefile* para auxiliar na execução do projeto. Assim,
navegue até a pasta raiz do projeto, aonde está o Makefile, e execute o comando
`make` para ver as opções disponíveis.

Antes de rodar o projeto, você precisará construir alguns containers localmente.
Para tanto, execute o comando:

`$ make build`

Em seguida basta executar:

`$ make iniciar`

E o projeto será iniciado localmente (ainda sem dados), acessível em `http://localhost/`.

## 3.1. Comandos úteis

Para parar de rodar o projeto:

`$ make parar`

Para interagir diretamente com o shell do django em execução:

`$ make django_shell`

Para acessar diretamente o banco de dados (Postgres):

`$ make psql`

Já para ver os logs da aplicação, usamos o comando `make logs`.

Outro comando que ajuda é ver o status de cada serviço. Para isso utilizamos o
comando:

`$ docker stack ps radar`

Se quiser acompanhar a situação dos serviços contiuamente, o comando `watch` é
seu melhor amigo (`-n1` significa que atualizado a cada segundo):

`$ watch -n1 docker stack ps radar`

Outro comando útil é o `docker service ls`, que dá mais informações sobre os
serviços em execução, independente da stack.

### 3.2. Rodar o projeto em produção

Para rodar o radar em produção você precisa definir algumas variáveis de
ambiente:

- `RADAR_IMAGE`: Qual a imagem docker que será utilizada no container principal
  (django) e no do celery. Inclusive com a TAG docker.
- `RADAR_NGINX_IMAGE`: Imagem (com TAG) a ser utilizada para o container do
  NGINX.
- `RADAR_IS_PRODUCTION`: Define que o projeto está sendo executado em ambiente
  de produção. Isso habilita o Cache do Django e coloca o DEBUG como 'FALSE'
- `RADAR_DB_PASSWORD`: Define a senha do banco de dados do radar em produção

Após exportar as variáveis acima, execute o comando:

`$ docker stack deploy -c radar.yml -c radar_prod.yml radar`

Obs: Caso o projeto já esteja rodando este comando irá atualizar os services em
execução.

A execução em produção difere da execução local em algumas configurações
(cache, jobs e logs). Para executar o Radar localmente utilizando essas
configurações de produção:

```
$ export RADAR_IS_PRODUCTION=True
$ make iniciar
```

## 4. Importação dos dados

Para importar os dados basta acessar a URL:

> http://localhost/importar/<nome-curto-da-casa-legislativa>/

Obs: Você precisará colocar login/senha para ter acesso a esta página. Este
login e senha são os criados com o seguinte comando:
`docker exec -it $(docker ps --filter name=radar_django -q) python manage.py createsuperuser`

Obs2: o comando `docker exec` recebe o "nome completo" (ou id) do container do
serviço. Como esse "nome/id" é gerado "aleatóriamente", usamos um pequeno
"truque" para descobri-lo (`docker ps --filter name=radar_<SERVICO> -q`).

Possíveis valores para `<nome-curto-da-casa-legislativa>`:

* Convenção Francesa: "conv"
* Camara Municipal de São Paulo: "cmsp"
* Senado: "sen"
* Câmara dos Deputados: "cdep"

Exemplo: para importar dados da Câmara Municipal de São Paulo basta acessar:

> http://localhost/importar/cmsp

Recomendamos inicialmente que você realize a importação dos dados Convenção
Nacional Francesa (uma casa legislativa fictícia).

Obs: todas as importações são relativamente rápidas, exceto a da Câmara dos
Deputados, que pode levar horas.

Além de importar via URL, uma maneira alternativa de importar os dados é:

```
$ make django_shell
>>> from importadores import importador
>>> importador.main(['cmsp', 'conv']) # pode passar outras casas na lista
```

- Criando novos importadores:

http://radarparlamentar.polignu.org/importadores/

## 6. Executando os testes localmente

Rode o comando:

Para testar a aplicação (testes python/django):

`$ make teste` ou `$ make teste T=aplicacao`

Para testar se há alguma migration que precisa ser criada/executada:

`$ make teste T=migrations`

Para executar os testes com linters:

`$ make linter` ou `$ make linter L=shellcheck`

`$ make linter L=http_link`

## 7. Criando e executando migrations

Após alterar algum model, rode os comandos

`$ make iniciar`

e

`$ make makemigrations`

Após gerar o arquivo da migration, pare a execução e inicie novamente:

`$ make parar`

e

`$ make iniciar`

E as alterações da nova migration terão sido aplicadas na base.