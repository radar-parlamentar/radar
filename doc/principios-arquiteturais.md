## Princípios arquiteturais para o Radar Parlamentar

O Radar Parlamentar possui escassez de colaboradores, portanto a arquitetura deve otimizar o uso dos colaboradores:

* 1.1 Evitar funcionalidades que demandem esforço operacional recorrente.
* 1.2 Priorizar facilidade de manutenção e experiência do desenvolvedor sobre outros aspectos, como desempenho por exemplo (claro, há exceções, como o tratamento do tempo para a execução da análise PCA).

Priorização:

* 2.1 As prioridades devem ser mais sobre aumentar a qualidade do Radar Parlamentar do que criar novas funcionalidades (fazer menos coisas pra fazer bem).
* 2.2 O mais interessante é agregar recursos à análise PCA do que criar novas análises separadas (estilo plenária).

Segurança:

* 3.1 Para evitar preocupações com segurança, o Radar Parlamentar não deve armazenar dados pessoais.
* 3.2 Evitar funcionalidades que tenham dados não-recuperáveis (dados de votações são recuperáveis porque podemos reimportá-los).

Grau reduzido de configurabilidade:

* 4.1 Mais opções de configuração tornam o sistema mais difícil de entender. Se a perspectiva de utilizar determinada flexibilidade for reduzida, remover a opção de configuração. Lembre-se: YAGNI (You Ain't Gonna Need It).

Outros:

* 5.1 Caso conflitantes, o rigor de análise e fidelidade aos dados deve se sobrepor à usabilidade.

Software livre

* 6.1 O Radar Parlamentar deve utilizar componentes que também sejam software livre.

Abertura / interoperabilidade

* 7.1 É desejável que os dados do Radar possam ser facilmente reaproveitados por outras aplicações.

* Língua

8.1 Manteremos o Radar Parlamentar em português, evitando um grande overhead de migração de língua.
