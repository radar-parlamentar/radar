# Cache no Radar Parlamentar

A análise PCA feita para o gráfico de radar é muito demorada. Por isso, armazenamos o resultado em um cache. Esse cache utiliza a biblioteca de cache de Django (`django.core.cache`). 

O cache é configurado pelo arquivo `settings.py`, sendo que essa configuração determina algumas coisas:

* O cache é configurado em todos os ambientes, mas
  * no ambiente de testes CACHE_TIMEOUT é sempre zero;
  * no ambiente de desenvolvimento CACHE_TIMEOUT é zero por padrão, mas pode ser sobrescrito em `radar_dev.yml`.
  * no ambiente de produção CACHE_TIMEOUT é infinito (nunca expira), pois fazemos a limpeza do cache de forma agendada ou manual (usando cron jobs ou via a URL `clear_cache`).
* Utilizamos o Memcache para salvar os dados do cache.
* Determinamos o endereço para o Django acessar o Memcache.
* Determinamos que o cache utiliza o [modo per-view](https://docs.djangoproject.com/en/3.1/topics/cache/#the-per-view-cache).
  * Isso significa que as views a serem cacheadas poderiam ser identificadas pelo decorator `@cache_page`.
  * Contudo, na prática, estamos controlando o armazenamento do cache programaticamente em `analises/view.py`, conforme o seguitne trecho:

```python
    response = cache.get(cache_key)
    if not response:
      ...
      cache.set(cache_key, response, settings.CACHE_TIMEOUT)
```

O cache é renovado diariamente às 1h (`CashRefresherJob` em `cron/jobs.py`). Com renovado, queremos dizer:  o cache é limpo e as análises são reexecutadas (renovando assim o cache). A execução de jobs (como o `CashRefresherJob`) por padrão é habilitada em produção e desabilitada no ambiente local.

O cache pode ser limpo manualmente invocando a url /radar/clear_cache.

Documentação do Django sobre cache: https://docs.djangoproject.com/en/3.1/topics/cache/

Obs: Há ainda o cache do Nginx, feito em produção somente para arquivos estáticos (diretório `static`). Essa configuração é feita no arquivo `nginx.conf`.

