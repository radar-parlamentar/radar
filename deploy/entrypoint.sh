#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Não checa nada se estivermos em ambiente de teste, visto que, POR ENQUANTO,
# estamos usando sqlite para testes.
export DB_HOST="${DB_HOST:-postgres}"
export DB_PORT="${DB_PORT:-5432}"

# Faz o setup da cron caso a entrada ainda não exista.
echo "Setuping cron"
# Executes 'manage.py runcrons' at each minute:
echo "* * * * * /usr/local/bin/python /radar/radar_parlamentar/manage.py runcrons >> /var/log/radar/cronjob.log 2>&1" > mycron
crontab mycron
rm mycron
/etc/init.d/cron start

# Setup postgresql auth file
echo "Setuping pgpass"
echo "postgres:5432:radar:radar:${RADAR_DB_PASSWORD}" > "${PGPASSFILE}"
chmod 0600 "${PGPASSFILE}"

# Coleta e comprime arquivos estáticos
python manage.py collectstatic --noinput
python manage.py compress

wait-for-it "$DB_HOST:$DB_PORT" -t 60
python manage.py migrate

case "$1" in
	celery)
		echo "Initializing celery."
		celery -A radar_parlamentar worker -l INFO --concurrency 1
		;;
  	*)
		echo "Default initialization."
		echo "Starting uwsgi"
		uwsgi --ini /radar/deploy/radar_uwsgi.ini
		;;
esac
